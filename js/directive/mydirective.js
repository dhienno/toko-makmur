app.directive('myDirective',function(){
  return {
    scope : {
      nama : '@',
      gaji_pokok : '@',
      tunjangan_lainnya : '@',
    },
    restrict: 'EA',
    template: '<b>{{nama}}</b>',
    replace: true,
    link: function(scope, elem, attrs){
      elem.bind('click',function() {
          scope.JKK=((24/100) * gaji_pokok);
         
      });
    }
  };
});
